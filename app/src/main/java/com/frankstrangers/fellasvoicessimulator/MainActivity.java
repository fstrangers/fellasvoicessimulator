package com.frankstrangers.fellasvoicessimulator;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.widget.RadioGroup;

import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextToSpeech t1;
    int maxLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        maxLength = getResources().getInteger(R.integer.max_length);

        final TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.textInputLayout1);
        final FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.button1);
        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.voices_radio_group);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                textInputLayout.setHint(getHint(checkedId));
            }
        });

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.ITALY);
                    floatingActionButton.setVisibility(View.VISIBLE);
                }
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t1.speak(getVoiceString(textInputLayout.getEditText().getText(), radioGroup.getCheckedRadioButtonId()), TextToSpeech.QUEUE_FLUSH, null);
            }
        });
    }

    private String getHint(int checked_id) {
        String text = "Talk like ";
        switch (checked_id) {
            case R.id.radioPatrizi:
                return text + "Andrea Patrizi";
            case R.id.radioDuz:
                return text + "Nicola Duz";
            case R.id.radioPeppe:
                return text + "Giuseppe Piazzese";
            case R.id.radioCat:
                return text + "Marco Cattaneo";
            case R.id.radioPeppone:
                return text + "Peppone";
            case R.id.radioZuck:
                return text + "Percentuale";
        }

        return "";
    }

    private String getVoiceString(Editable editable, int checked_id) {
        String text = editable != null ? editable.toString() : "";

        switch (checked_id) {
            case R.id.radioPatrizi:
                return "Oi boss, ah si? " + text;
            case R.id.radioDuz:
                return "Copia e incooollaaa ," + text;
            case R.id.radioPeppe:
                return peppeText(text);
            case R.id.radioCat:
                return catText(text);
            case R.id.radioPeppone:
                return pepponeText(text);
            case R.id.radioZuck:
                return zuckText(text);
        }

        return "";
    }

    private String zuckText(String text) {
        if (text.length() == 0)
            return "Mi serve la percentuale di completamento!";

        return "La percentuale di completamento della seguente barra di testo è di : " + (((text.toCharArray().length) / ((double) maxLength)) * 100f) + "%";
    }

    private String peppeText(String text) {
        Random rand = new Random();

        switch (rand.nextInt(6) + 1) {
            case 1:
                return "Non ci gredol";
            case 2:
                return "Cooooool caaaaavo!";
            case 3:
                return "Teee lo faccio vedere io ora!!!";
            case 4:
                return "Oggi birra media e rana pescatrice!";
            case 5:
                return "Oggi prepariamo una bella erre!";
            default:
                return text;
        }
    }

    private String catText(String text) {
        Random rand = new Random();

        switch (rand.nextInt(4) + 1) {
            case 1:
                return "Borrrrraccia!";
            case 2:
                return "Oggi pollo al kerry!";
            case 3:
                return "Ham! Ham! Ham!";
            default:
                return text + ",...borrrrraccia!";
        }
    }

    private String pepponeText(String text) {
        return "Pepppone!";
    }

    public void onPause() {
        if (this.isFinishing()) {
            if (t1 != null) {
                t1.stop();
                t1.shutdown();
            }
        }
        super.onPause();
    }
}
